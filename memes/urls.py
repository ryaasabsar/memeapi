from django.urls import path, include
from .views import home

app_name = "memes"

urlpatterns = [
    path('', home, name='home')
]