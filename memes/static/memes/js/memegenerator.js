$(document).ready(function(){
    memeGenerator();

    function memeGenerator() {
        var memesCount =  Math.floor(Math.random() * 10) + 6
        var dataRes = ""
        
        
        $.ajax({
            url : "https://okto-memes.herokuapp.com/gimme/" + memesCount,
            error: function(){
                alert("Something went wrong, try again.")
            },
            success: function(data){
                for(var counter=0; counter<data.count; counter++){
                    dataRes += '<div class="container max-width w35-rem mt-5">';
                    dataRes += '<div class="card w-80-auto bg-dark-blue b-ablue">';
                    
                    dataRes += '<a href="' + data.memes[counter].link + '"><img src="' + data.memes[counter].url + '" class="card-img-top center-cropped"></a>';
                    dataRes += '<div class="row mt-4 ml-4">';
                    dataRes += '<div class="col-8">';
                    dataRes += '<p class="fw-700 ">' + data.memes[counter].title + '</p>';
                    dataRes += '<p class="fw-400 mt--1"> by ' + data.memes[counter].author + '</p>';
                    dataRes += '</div>';
                    dataRes += '<div class="col-2 ml-auto mr-2">';
                    dataRes += '<img src="https://i.imgur.com/aqWiVuW.png" style="max-width:1rem;">';
                    dataRes += '</div>';
                    dataRes += '</div>';
                    dataRes += '</div>';
                    dataRes += '</div>';

                }
                $("#memes-container").append(dataRes)
            },

        })
    }

    $("#more-button").click(function(){
        memeGenerator();
    })
})


